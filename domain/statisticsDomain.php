<?php

include('../../data/datasource.php');

function getWordsStatistics(){

    $searchProductsResult = getWordsStatisticsMysql();
    $orderProductsByWords;
    $result;
    $productsIdSize = count($searchProductsResult["orderId"]);
    $productsWordsSize = count($searchProductsResult["words"]);
    
    for($i = 0; $i < $productsWordsSize; $i++){
        $id = $searchProductsResult["words"][$i]["product_id"];
        $word = $searchProductsResult["words"][$i]["palabras"];
        $orderProductsByWords[$id] .= $word. " ";
    }

    for($o = 0; $o < $productsIdSize; $o++){
        $id = $searchProductsResult["orderId"][$o];
        $titulo = $searchProductsResult["titulos"][$id];
        $result[$titulo][] = $orderProductsByWords[$id];
    }
    
    return $result;
    
}

?>