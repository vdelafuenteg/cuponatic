<?php

function searchProductsByWordMysql($keywords){
    include('conections/mysqlConections.php');
    
    $result = $mysqli->query('SELECT * FROM products where titulo like "%'.$keywords.'%" limit 50');
    $resultArray = __searchMysqliToArray($mysqli, $result, $keywords);
    return json_encode($resultArray);
    
}

function __searchMysqliToArray($mysqli, $result, $keywords){

    $resultArray;
    $productsIdForMarkSearchProduct;
    $productsIdForMarkKeywordsProduct;
    while($row = $result->fetch_assoc()) {
        $resultArray[] = $row;
        if(empty($productsIdForMarkSearchProduct)){
            $productsIdForMarkSearchProduct = "(". $row["id"].",'".$row["titulo"]."',0)";
            $productsIdForMarkKeywordsProduct = "(". $row["id"].",'".$keywords."')";
        }
        $productsIdForMarkSearchProduct = $productsIdForMarkSearchProduct. "," . "(". $row["id"].",'".$row["titulo"]."',0)";
        $productsIdForMarkKeywordsProduct = $productsIdForMarkKeywordsProduct. ",". "(". $row["id"].",'".$keywords."')";
    }
    __markSearchProductMysql($mysqli, $productsIdForMarkSearchProduct);
    __markWordsSearchProductMysql($mysqli, $productsIdForMarkKeywordsProduct);
    mysqli_close($mysqli);
    return $resultArray;
}

function __getMostWantedReturnIdMysql($mysqli){
    $result = $mysqli->query('SELECT products_id, titulo FROM mas_buscados ORDER BY cantidad_buscado DESC limit 4');
    return $result;
}

function getWordsStatisticsMysql(){
    include('conections/mysqlConections.php');
    $mysqlIdMostWanted = __getMostWantedReturnIdMysql($mysqli);
    $idForMysqlSearch;
    $titulosProducts;
    while($row = $mysqlIdMostWanted->fetch_assoc()) {
        $idForMysqlSearch[] = $row["products_id"];
        $titulosProducts[$row["products_id"]] = $row["titulo"];
    }
    $idForMysqlSearchForMysql = implode(",",$idForMysqlSearch);

    $result = $mysqli->query('
        SELECT product_id, palabras, count(palabras) as count
        FROM mas_buscados_keywords
        WHERE product_id IN ('.$idForMysqlSearchForMysql.')
        GROUP BY product_id, palabras
        ORDER BY count DESC
    ');
    $mostWordsStatistics;
    while($row = $result->fetch_assoc()) {
        $mostWordsStatistics[] = $row;
    }
    $statistics["titulos"] = $titulosProducts;
    $statistics["orderId"] = $idForMysqlSearch;
    $statistics["words"] = $mostWordsStatistics;

    return $statistics;
}

function __markSearchProductMysql($mysqli, $products){
    $mysqli->query("INSERT INTO mas_buscados (products_id, titulo, cantidad_buscado) VALUES $products ON DUPLICATE KEY UPDATE cantidad_buscado = cantidad_buscado + 1");
}

function __markWordsSearchProductMysql($mysqli, $keywords){
    $mysqli->query("INSERT INTO mas_buscados_keywords (product_id, palabras) VALUES $keywords");
}

?>